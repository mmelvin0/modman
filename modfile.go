package modman

import (
	"crypto/md5"
	"hash"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

// ModFile represents a mod JAR file.
type ModFile struct {

	// path is the fully qualified path to the mod JAR file.
	Path string

	// Info is a set of info structs describing the mods contained in the JAR file.
	Info ModInfoSet

	// Size is the file size of the JAR file.
	Size int64

	// md5 if the MD5 hash of the JAR file.
	MD5 []byte
}

// IsModFile checks to see if a file looks like a valid mod.
func IsModFile(path string) bool {
	if !isZipFile(path) {
		return false
	}
	mods, err := NewModInfoSet(path)
	if err != nil {
		return false
	}
	// there must be at least one entry in mcmod.info with an empty parent
	return mods.Root() != nil
}

// NewModFile returns a ModFile pointing at a specific file path.
func NewModFile(path string) (*ModFile, error) {
	info, err := NewModInfoSet(path)
	if err != nil {
		return nil, err
	}
	stat, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	sum, err := hashFile(md5.New(), path)
	return &ModFile{path, *info, stat.Size(), sum}, nil
}

// FindModFiles scans a file or directory for mods and returns a slice of ModFile(s)
func FindModFiles(path string, recurse bool) ([]ModFile, error) {
	parent, children, err := splitModFilePath(path)
	if err != nil {
		return nil, err
	}
	result := make([]ModFile, 0)
	err = findModFiles(parent, children, recurse, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// findModFiles is the internal scanning implementation for the exported FindModFiles.
func findModFiles(parent string, children []os.FileInfo, recurse bool, result *[]ModFile) error {
	for _, child := range children {
		path := filepath.Join(parent, child.Name())
		info, err := os.Stat(path)
		if err != nil {
			return err
		}
		if info.IsDir() {
			if recurse {
				kids, err := ioutil.ReadDir(path)
				if err != nil {
					return err
				}
				findModFiles(path, kids, recurse, result)
			}
		} else if IsModFile(path) {
			mod, err := NewModFile(path)
			if err != nil {
				return err
			}
			*result = append(*result, *mod)
		}
	}
	return nil
}

// hashFile is a convenience for calculating the hash of a file.
func hashFile(hasher hash.Hash, path string) ([]byte, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	_, err = io.Copy(hasher, file)
	if err != nil {
		return nil, err
	}
	return hasher.Sum(nil), nil
}

// isZipFile determines if a file looks like a ZIP archive.
func isZipFile(path string) bool {
	file, err := os.Open(path)
	if err != nil {
		return false
	}
	defer file.Close()
	// to be a zip file, the first four bytes of the file must match the condition below
	// thanks to https://github.com/h2non/filetype/blob/master/matchers/archive.go
	// i didn't need the whole library just this teeny bit
	buffer := make([]byte, 4)
	read, err := file.Read(buffer)
	if err != nil {
		return false
	}
	return read >= 4 && len(buffer) >= 4 &&
		buffer[0] == 0x50 &&
		buffer[1] == 0x4b &&
		(buffer[2] == 0x3 || buffer[2] == 0x5 || buffer[3] == 0x7) &&
		(buffer[3] == 0x4 || buffer[3] == 0x6 || buffer[3] == 0x8)
}

// splitModFilePath splits the path into parent and child parts.
// If the path is a file, the parent directory and single os.FileInfo is returned.
// If the path is a directory, the directory path and children are returned.
func splitModFilePath(path string) (string, []os.FileInfo, error) {
	info, err := os.Stat(path)
	if err != nil {
		return "", nil, err
	}
	if info.IsDir() {
		children, err := ioutil.ReadDir(path)
		if err != nil {
			return "", nil, err
		}
		return path, children, nil
	} else {
		return filepath.Dir(path), []os.FileInfo{info}, nil
	}
}
