package curseforge

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
	"github.com/pkg/errors"
	"strconv"
	"strings"
)

// URL is the default to access Minecraft CurseForge
const URL = "https://minecraft.curseforge.com"

// Project represents a CurseForge project
type Project struct {
	// ID is the CurseForge ID of the project
	ID int
	// Slug is the project slug
	Slug string
	// Name is the proper title of the project
	Name string
	// Owner is the CurseForge user who currently owns the project
	Owner string
	// License is the name of the license used by the project
	License string
	// Created is the timestamp at which the project was created
	Created int
	// Updated is the timestamp at which the project was last modified
	Updated int
	// Downloads is the number of times a file has been downloaded from this project
	Downloads int
}

// Scrape is a configurable context for scraping operations
type Scrape struct {
	// Collector is the web scraper
	Collector *colly.Collector
	// URL is the URL to access access Minecraft CurseForge with
	URL string
	// Warnings that occur while scraping
	Warnings []error
}

// NewScrape creates/updates a Scrape with default values
func NewScrape(scrape *Scrape) *Scrape {
	if scrape == nil {
		scrape = &Scrape{}
	}
	if scrape.Collector == nil {
		scrape.Collector = colly.NewCollector()
	}
	if scrape.URL == "" {
		scrape.URL = URL
	}
	if scrape.Warnings == nil {
		scrape.Warnings = make([]error, 0)
	}
	return scrape
}

// ScrapeProjectSlug creates/updates a project by scraping the CurseForge URL corresponding to the given project slug
func ScrapeProjectSlug(slug string, project *Project, scrape *Scrape) (*Project, error) {
	if project == nil {
		project = &Project{Slug: slug}
	} else {
		project.Slug = slug
	}
	scrape = NewScrape(scrape)
	url := fmt.Sprintf("%s/projects/%s", scrape.URL, slug)
	err := scrapeProject(url, scrape, project)
	return project, err
}

// scrapeProject performs the actual scraping operation
func scrapeProject(url string, scrape *Scrape, project *Project) error {
	err := error(nil)
	warnings := make(chan error)
	go func() {
		defer close(warnings)
		scrapeProjectSetup(scrape, project, warnings)
		err = scrape.Collector.Visit(url)
	}()
	for warning := range warnings {
		scrape.Warnings = append(scrape.Warnings, warning)
	}
	return err
}

// scrapeProjectSetup attaches colly listeners needed to scrape a project
func scrapeProjectSetup(scrape *Scrape, project *Project, warnings chan<- error) {
	scrape.Collector.OnHTML("h1.project-title", func(element *colly.HTMLElement) {
		project.Name = strings.TrimSpace(element.Text)
	})
	scrape.Collector.OnHTML(".project-members .owner a", func(element *colly.HTMLElement) {
		project.Owner = strings.TrimSpace(element.Text)
	})
	scrape.Collector.OnHTML(".project-details li", func(element *colly.HTMLElement) {
		node := element.DOM.Find(".info-data")
		data := strings.TrimSpace(node.Text())
		label := strings.TrimSpace(element.DOM.Find(".info-label").Text())
		switch label {
		case "Project ID":
			checkWarning(extractInt(data, &project.ID), warnings)
		case "Created":
			checkWarning(extractIntAttr(node.Find(".standard-date"), "data-epoch", &project.Created), warnings)
		case "Last Released File":
			checkWarning(extractIntAttr(node.Find(".standard-date"), "data-epoch", &project.Updated), warnings)
		case "Total Downloads":
			checkWarning(extractInt(data, &project.Downloads), warnings)
		case "License":
			project.License = data
		default:
			warnings <- errors.New(fmt.Sprintf("unknown project detail label: %s", label))
		}
	})
}

// checkWarning sends all non-nil error to the passed warnings channel
func checkWarning(err error, warnings chan<- error) {
	if err != nil {
		warnings <- err
	}
}

// extractInt parses integer strings
func extractInt(input string, output *int) error {
	input = strings.Replace(input, ",", "", -1)
	value, err := strconv.Atoi(input)
	if err != nil {
		return err
	}
	*output = value
	return nil
}

// extractIntAttr parses integer attributes
func extractIntAttr(node *goquery.Selection, attr string, output *int) error {
	data, ok := node.Attr(attr)
	if !ok {
		return errors.New(fmt.Sprintf("no such attribute: %s", attr))
	}
	return extractInt(data, output)
}
