# "production" build - smaller / less debuggable
# i wonder if this works at all? - i made it on windows - too lazy to scp over to arch to test
# damnit gnu make why you be different

.PHONY: clean

modman:
	go get -d -v ./...
	go build -i -v -x -ldflags="-s -w" "./cmd/modman"
	upx modman

clean:
	rm modman
