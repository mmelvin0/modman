package modman

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"os"
)

type Repo struct {
	db *sql.DB
	projects map[string]*Project
}

func OpenRepo(file string) (*Repo, error) {
	db, err := openDb(file)
	if err != nil {
		return nil, err
	}
	return &Repo{db, make(map[string]*Project)}, nil
}

func (r *Repo) GetProject(slug string, create bool) (*Project, error) {
	p, exists := r.projects[slug]
	if exists {
		return p, nil
	}
	p = &Project{slug: slug}
	err := r.db.
		QueryRow("SELECT project_id, slug, name, owner, license, created, updated, downloads FROM project WHERE slug = ?", slug).
		Scan(&p.id, &p.slug, &p.name, &p.owner, &p.license, &p.created, &p.updated, &p.downloads)
	if err != nil && (err != sql.ErrNoRows || !create) {
		return nil, err
	}
	r.projects[slug] = p
	return p, nil
}

func (r *Repo) SaveProject(p *Project) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	exists := false
	if err := tx.QueryRow("SELECT COUNT(1) FROM project WHERE project_id = ?", p.id).Scan(&exists); err != nil {
		return err
	}
	var query string
	if exists {
		query = "UPDATE project SET slug = ?, name = ?, owner = ?, license = ?, created = ?, updated = ?, downloads = ? WHERE project_id = ?"
	} else {
		query = "INSERT INTO project (slug, name, owner, license, created, updated, downloads, project_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
	}
	if _, err := tx.Exec(query, p.slug, p.name, p.owner, p.license, p.created, p.updated, p.downloads, p.id); err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

func openDb(file string) (*sql.DB, error) {
	create := false
	_, err := os.Stat(file)
	if err != nil {
		if os.IsNotExist(err) {
			create = true
		} else {
			return nil, err
		}
	}
	db, err := sql.Open("sqlite3", file)
	if err != nil {
		return nil, err
	}
	if create {
		if err := createDb(db); err != nil {
			return nil, err
		}
	}
	return db, nil
}

func createDb(db *sql.DB) error {
	for _, query := range []string{
		"CREATE TABLE project (project_id INT, slug TEXT, name TEXT, owner TEXT, license TEXT, created INT, updated INT, downloads INT)",
	} {
		if _, err := db.Exec(query); err != nil {
			return err
		}
	}
	return nil
}
