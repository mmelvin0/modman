package main

import "gitlab.com/mmelvin0/modman"

func main() {
	// proposed sub-commands:
	// + info
	// + outdated
	// + search
	// + update
	modman.RunCLI()
}
