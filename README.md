# modman

The `modman` project aspires to be an ad-hoc package manager for [Minecraft Forge](https://github.com/MinecraftForge/MinecraftForge) mods.

It's currently in its infancy as I fantasize about being a Golang rock star.

# Build

At its simplest:

```bash
go build ./cmd/modman
```

This makes a huge executable though. To trim that down a bit (at the expense of debugability)...

```bash
go build -ldflags="-s -w" "./cmd/modman"
upx -q modman.exe
```

The second step requires [UPX](https://upx.github.io/).
