package modman

type Project struct {
	id int
	slug string
	name string
	owner string
	license string
	created int
	updated int
	downloads int
}
