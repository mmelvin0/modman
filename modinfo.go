package modman

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"regexp"
)

// ModInfo represents a single entry from a mcmod.info file.
// Generally each mcmod.info file should contain at least one entry.
// + https://github.com/MinecraftForge/FML/wiki/FML-mod-information-file
// + https://mcforge.readthedocs.io/en/latest/gettingstarted/structuring/#the-mcmodinfo-file
type ModInfo struct {

	// ID is the identifier of the mod.
	// Valid mod IDs are lowercase, start with a letter, and contain only letters and numbers?
	ID string `json:"modid"`

	// Name is the short human-readable name of the mod.
	Name string `json:"name"`

	// Description is a longer summary of the mod.
	Description string `json:"name,omitempty"`

	// Version is the version of the mod
	Version string `json:"version,omitempty"`

	// MCVersion is the Minecraft version this mod is compatible with.
	MCVersion string `json:"mcversion,omitempty"`

	// URL is the web site for the mod.
	URL string `json:"url,omitempty"`

	// UpdateURL is a... update URL?
	UpdateURL string `json:"updateUrl,omitempty"`

	// UpdateJSON is a URL to a version.json file
	UpdateJSON string `json:"updateJSON,omitempty"`

	// Author is a list of the mod's author(s).
	Author []string `json:"authorList,omitempty"`

	// Credits for the mod
	Credits string `json:"credits,omitempty"`

	// Logo for the mod
	Logo string   `json:"logoFile,omitempty"`

	// Screenshot is a list of images (ostensibly screenshots.)
	Screenshot []string `json:"screenshots,omitempty"`

	// Parent is the ID of this mod's parent, or an empty string for the root mod.
	Parent string `json:"parent,omitempty"`

	// UseDep means this mod uses mcmod.info for dependencies instead of @Mod Java annotation.
	UseDep bool `json:"useDependencyInformation,omitempty"`

	// Dependency is a list of mods that must be present to load Minecraft with this mod enabled.
	Dependency []string `json:"requiredMods,omitempty"`

	// LoadAfter is a list of (optional) mods for this mod to load after
	LoadAfter []string `json:"dependencies,omitempty"`

	// LoadBefore is a list of (optional) mods for this mod to load before
	LoadBefore []string `json:"dependants,omitempty"`

}

// ModInfoSet represents a set of ModInfo(s).
// Generally it has a one-to-one relationship to a mod file.
type ModInfoSet struct {

	// Mods is the list of mod entries from mcmod.info
	Mods []ModInfo `json:"modList"`
}

// Root gets the root mod in a collection.
// If there is no root mod, it returns nil.
func (mods *ModInfoSet) Root() *ModInfo {
	for _, mod := range mods.Mods {
		if len(mod.Parent) <= 0 {
			return &mod
		}
	}
	return nil
}

// NewModInfoSet creates a ModInfoSet from a JAR file containing a top-level mcmod.info file.
func NewModInfoSet(path string) (*ModInfoSet, error) {
	reader, err := zip.OpenReader(path)
	if err != nil {
		return nil, err
	}
	defer reader.Close()
	for _, file := range reader.File {
		if file.Name == "mcmod.info" {
			return readModInfo(file)
		}
	}
	return nil, errors.New(fmt.Sprintf("%s: does not contain top-level mcmod.info file", path))
}

// readModInfo makes a ModInfoSet from a mcmod.info ZIP entry.
func readModInfo(file *zip.File) (*ModInfoSet, error) {
	reader, err := file.Open()
	if err != nil {
		return nil, err
	}
	defer reader.Close()
	buffer, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}
	mods, err := parseModInfo(fixJson(buffer))
	if err != nil {
		return nil, err
	}
	return mods, nil
}

// parseModInfo unmarshals mcmod.info JSON.
func parseModInfo(buffer []byte) (*ModInfoSet, error) {
	mods := ModInfoSet{}
	// first try to unmarshal as a JSON array (most common format it seems
	err := json.Unmarshal(buffer, &mods.Mods)
	if err == nil {
		// return early if successful
		return &mods, nil
	}
	// next try to umarshal as an object with array property "modList"
	err = json.Unmarshal(buffer, &mods)
	if err != nil {
		return nil, err
	}
	return &mods, nil
}

// doubleQuoted is a regular expression for use in fixJson.
var doubleQuoted = regexp.MustCompile(`(?m:"([^\\"]|\\")*")`)

// newLine is a byte slice matching an actual new line.
var newLine = []byte("\n")

// newLineEscaped is a byte slice representing a backslash-escaped new line.
var newLineEscaped = []byte(`\n`)

// fixJson replaces actual new lines with \n escape sequences.
// This is a common mistake in mcmod.info JSON files that the Go JSON decoder rejects but other decoders seem to tolerate.
func fixJson(buffer []byte) []byte {
	return doubleQuoted.ReplaceAllFunc(buffer, func(match []byte) []byte {
		if bytes.Contains(match, newLine) {
			return bytes.Replace(match, newLine, newLineEscaped, -1)
		} else {
			return match
		}
	})
}
