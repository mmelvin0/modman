rem "production" build - smaller / less debuggable
rem on windows 10 x64 golang 1.11:
rem go build = 23 MB
rem go build + ldflags + upx = 3.7 MB
rem damnit microsoft why you be different
go get -d -v ./...
go build -i -v -x -ldflags="-s -w" ".\cmd\modman"
upx modman.exe
