package modman

import (
	"fmt"
	"gitlab.com/mmelvin0/modman/curseforge"
	"log"
	"os"
	"path/filepath"
)

// RunCLI runs the modman command line interface.
func RunCLI() {
	err := example3()
	if err != nil {
		panic(err)
	}
}

func example1() error {
	var dir string
	var err error
	if len(os.Args) >= 2 {
		dir = os.Args[1]
	} else {
		dir, err = os.Getwd()
		if err != nil {
			return err
		}
	}
	files, err := FindModFiles(dir, true)
	if err != nil {
		return err
	}
	for _, file := range files {
		log.Printf("%s: %s (%s)", filepath.Base(file.Path), file.Info.Root().ID, file.Info.Root().Version)
	}
	return nil
}

func example3() error {
	scrape := curseforge.NewScrape(nil)
	project, err := curseforge.ScrapeProjectSlug("tinkers-construct", nil, scrape)
	if len(scrape.Warnings) > 0 {
		for _, warning := range scrape.Warnings {
			log.Println(fmt.Sprintf("warning: %s", warning.Error()))
		}
	}
	if err != nil {
		return err
	}
	log.Println(project)
	return nil
}
